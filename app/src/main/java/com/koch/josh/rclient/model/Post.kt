package com.koch.josh.rclient.model

/**
 * Created by Josh on 4/19/2016.
 */

data class RedditResponse(val after: String,
    val before: String,
    val news: List<Post>)

data class Post(val title: String,
    val author: String,
    val subreddit: String,
    val num_comments: Int,
    val score: Int,
    val created: Long,
    val thumbnail: String,
    val permalink: String,
    val url: String)

/*
class NetworkManager() {

  private val api: RestAPI = RestAPI()

  fun getPosts(after: String, limit: String = "10"): Observable<RedditResponse> {
    return Observable.create { subscriber ->
      val callResponse = api.getNews(after, limit)
      val response = callResponse.execute()

      if (response.isSuccessful) {
        val dataResponse = response.body().data
        val news = dataResponse.children.map {
          val item = it.data
          Post(item.title, item.author, item.subreddit, item.num_comments, item.score,
              item.created, item.thumbnail, item.permalink, item.url)
        }
        val redditNews = RedditResponse(
            dataResponse.after ?: "",
            dataResponse.before ?: "",
            news)

        subscriber.onNext(redditNews)
        subscriber.onCompleted()
      } else {
        subscriber.onError(Throwable(response.message()))
      }
    }
  }
}*/
