package com.koch.josh.rclient.user

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter

/**
 * Created by Josh on 5/4/2016.
 */
class UserPresenter: MvpBasePresenter<UserView>() {
  override fun attachView(view: UserView?) {
    super.attachView(view)
  }

  override fun detachView(retainInstance: Boolean) {
    super.detachView(retainInstance)
  }
}