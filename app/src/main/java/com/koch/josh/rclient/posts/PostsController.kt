package com.koch.josh.rclient.posts

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.bindView
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler
import com.hannesdorfmann.mosby.conductor.viewstate.lce.MvpLceViewStateController
import com.hannesdorfmann.mosby.mvp.viewstate.layout.MvpViewStateFrameLayout
import com.hannesdorfmann.mosby.mvp.viewstate.lce.LceViewState
import com.hannesdorfmann.mosby.mvp.viewstate.lce.data.RetainingLceViewState
import com.koch.josh.rclient.R
import com.koch.josh.rclient.R.id
import com.koch.josh.rclient.R.layout
import com.koch.josh.rclient.SimpleDividerItemDecoration
import com.koch.josh.rclient.user.UserController
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.controller_posts.view.bottom_navigation
import kotlinx.android.synthetic.main.controller_posts.view.container_0
import kotlinx.android.synthetic.main.controller_posts.view.container_1
import kotlinx.android.synthetic.main.controller_posts.view.container_2
import kotlinx.android.synthetic.main.controller_posts.view.container_3
import kotlinx.android.synthetic.main.controller_posts.view.contentView
import kotlinx.android.synthetic.main.controller_posts.view.swipeRefreshLayout
import net.dean.jraw.auth.AuthenticationManager
import net.dean.jraw.auth.AuthenticationState
import net.dean.jraw.models.Listing
import net.dean.jraw.models.Submission
import net.dean.jraw.models.Submission.ThumbnailType
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.warn

/**
 * Created by Josh on 4/26/2016.
 */
class PostsController : PostView, MvpLceViewStateController<RecyclerView, List<Submission>, PostView, PostPresenter>(), AnkoLogger {

  //region Variables
  /* -------------- Variables -------------- */
  private val KEY_CURRENT_TAB: String = "PostsController.currentTab"

  private lateinit var adapter: PostAdapter
  private var mCurrentTab: Int = 0

  lateinit var swipeRefreshLayout: SwipeRefreshLayout
  lateinit var mChildContainers: Array<ViewGroup>
  lateinit var mBottomNavigation: AHBottomNavigation
  lateinit var mVisibleContainer: ViewGroup
  //endregion

  //region Mosby Override Methods
  /* -------------- Mosby -------------- */

  override fun createPresenter(): PostPresenter = PostPresenter()

  override fun createViewState(): LceViewState<List<Submission>, PostView> = RetainingLceViewState()

  override fun getData(): List<Submission>? {
    return adapter.items
  }

  override fun getErrorMessage(e: Throwable?,
      pullToRefresh: Boolean): String? = "There was an error"

  override fun showLoading(pullToRefresh: Boolean) {
    if (pullToRefresh) {
      loadingView.visibility = MvpViewStateFrameLayout.GONE
      errorView.visibility = MvpViewStateFrameLayout.GONE
      swipeRefreshLayout.visibility = MvpViewStateFrameLayout.VISIBLE
      swipeRefreshLayout.post {
        swipeRefreshLayout.isRefreshing = true
      }
    } else {
      loadingView.visibility = MvpViewStateFrameLayout.VISIBLE
      errorView.visibility = MvpViewStateFrameLayout.GONE
      swipeRefreshLayout.visibility = MvpViewStateFrameLayout.GONE
    }
  }

  override fun showContent() {
    if (isRestoringViewState) {
      swipeRefreshLayout.visibility = MvpViewStateFrameLayout.VISIBLE
      errorView.visibility = MvpViewStateFrameLayout.GONE
      loadingView.visibility = MvpViewStateFrameLayout.GONE
    } else {
      swipeRefreshLayout.alpha = 0f
      swipeRefreshLayout.visibility = MvpViewStateFrameLayout.VISIBLE
      swipeRefreshLayout.animate().alpha(1f).start()
      loadingView.animate().alpha(
          0f).withEndAction { loadingView.visibility = MvpViewStateFrameLayout.GONE; loadingView.alpha = 1f }
          .start()

      errorView.visibility = MvpViewStateFrameLayout.GONE
    }
    swipeRefreshLayout.isRefreshing = false
  }

  override fun showError(e: Throwable?, pullToRefresh: Boolean) {
    if (pullToRefresh) {
      swipeRefreshLayout.visibility = MvpViewStateFrameLayout.VISIBLE
      errorView.visibility = MvpViewStateFrameLayout.GONE
      loadingView.visibility = MvpViewStateFrameLayout.GONE
    } else {
      swipeRefreshLayout.visibility = MvpViewStateFrameLayout.GONE
      loadingView.visibility = MvpViewStateFrameLayout.GONE
      errorView.visibility = MvpViewStateFrameLayout.VISIBLE
    }
    swipeRefreshLayout.isRefreshing = false
  }

  override fun setData(data: List<Submission>?) {
    adapter.items = data
    adapter.notifyDataSetChanged()
    loadingView.visibility = View.GONE
  }

  override fun loadData(pullToRefresh: Boolean) {
    if(AuthenticationManager.get().checkAuthState() == AuthenticationState.READY) {
      presenter.loadPosts(pullToRefresh)
    }else {
      warn("Not authenticated")
    }
  }

  //endregion

  override fun onRestoreInstanceState(savedInstanceState: Bundle) {
    super.onRestoreInstanceState(savedInstanceState)

    mCurrentTab = savedInstanceState.getInt(KEY_CURRENT_TAB)
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)

    outState.putInt(KEY_CURRENT_TAB, mCurrentTab)
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {

    val view = inflater.inflate(layout.controller_posts, container, false)
    val contentView: RecyclerView = view.contentView //findViewById(id.contentView) as RecyclerView
    swipeRefreshLayout = view.swipeRefreshLayout
    mChildContainers = arrayOf(view.container_0, view.container_1, view.container_2, view.container_3)
    mBottomNavigation = view.bottom_navigation
    mVisibleContainer = view.container_0

    setupBottomNavigation()

    adapter = PostAdapter(inflater)

    contentView.adapter = adapter
    contentView.layoutManager = LinearLayoutManager(activity)
    contentView.addItemDecoration(SimpleDividerItemDecoration(container.context))

    errorView.visibility = View.GONE
    loadingView.visibility = View.GONE

    swipeRefreshLayout.setOnRefreshListener { loadData(true) }

    return view
  }

  //region Bottom Navigation
  private fun setupBottomNavigation() {
    mBottomNavigation.addItem(AHBottomNavigationItem(R.string.home, R.drawable.ic_home_black_24dp, R.color.blue_300))
    mBottomNavigation.addItem(AHBottomNavigationItem(R.string.subreddits, R.drawable.ic_subreddit_black_24dp, R.color.green_300))
    mBottomNavigation.addItem(AHBottomNavigationItem(R.string.messages, R.drawable.ic_mail_outline_24dp, R.color.red_300))
    mBottomNavigation.addItem(AHBottomNavigationItem(R.string.user, R.drawable.ic_user, R.color.yellow_300))

    mBottomNavigation.setOnTabSelectedListener { position, wasSelected -> onNavTabSelected(position) }

    mBottomNavigation.isColored = true
    onNavTabSelected(mCurrentTab)
  }

  private fun onNavTabSelected(position: Int) {
    if(mCurrentTab != position) {
      mCurrentTab = position

      val oldContainer: ViewGroup = mVisibleContainer
      mVisibleContainer = mChildContainers[position]
      mVisibleContainer.visibility = View.VISIBLE

      getChildRouter(oldContainer, null).setHandlesBack(false)

      val containerAnimations: AnimatorSet = AnimatorSet()
      containerAnimations.play(ObjectAnimator.ofFloat(mVisibleContainer, View.ALPHA, 1.toFloat()))
      containerAnimations.play(ObjectAnimator.ofFloat(oldContainer, View.ALPHA, 0.toFloat()))
      containerAnimations.addListener(object: AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator?) {
          oldContainer.visibility = View.GONE
        }
      })
      containerAnimations.start()

      val childRouter: Router = getChildRouter(mChildContainers[position], null).setPopsLastView(false)
      childRouter.setHandlesBack(true)
      if(!childRouter.hasRootController()) {
        childRouter.setRoot(RouterTransaction.builder(PostsController()).build())
      }
      when(position){
        3 -> childRouter.pushController(RouterTransaction.builder(UserController())
            .pushChangeHandler(HorizontalChangeHandler())
            .popChangeHandler(HorizontalChangeHandler())
            .build())
      }
    }
  }
  //endregion

  //region PostAdapter/Holder
  class PostHolder(v: View) : ViewHolder(v) {
    val thumbnail: ImageView by bindView(id.thumbnail)
    val title: TextView by bindView(id.title)
    val author: TextView by bindView(id.author)
    val subreddit: TextView by bindView(id.subreddit)
    val num_comments: TextView by bindView(id.num_comments)
    val score: TextView by bindView(id.score)
    //val created: TextView by bindView(R.id.created)
    //val domain: TextView by bindView(R.id.domain)

    lateinit var submission: Submission

    /*    init {
          itemView.setOnClickListener {
            clickCallback(post)
          }
        }*/
  }

  class PostAdapter(private val inflater: LayoutInflater) : Adapter<PostHolder>() {

    var items: List<Submission>? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PostHolder = PostHolder(
        inflater.inflate(layout.item_post, parent, false))

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
      val submission = items!![position]
      holder.submission = submission
      holder.title.text = submission.title
      holder.author.text = submission.author
      holder.subreddit.text = submission.subredditName
      holder.num_comments.text = submission.commentCount.toString()
      holder.score.text = submission.score.toString()
      //holder?.created?.text = submission.created.toString()
      //holder?.domain?.text = submission.domain
      if (submission.thumbnailType == ThumbnailType.NONE) {
        holder.thumbnail.visibility = View.GONE
      } else if (submission.thumbnailType == ThumbnailType.URL){
        holder.thumbnail.visibility = View.VISIBLE
        Picasso.with(holder.itemView?.context).load(submission.thumbnail).fit().centerCrop().into(
            holder.thumbnail)
      }
    }

    override fun getItemCount(): Int = if (items == null) 0 else items!!.size
  }
  //endregion
}