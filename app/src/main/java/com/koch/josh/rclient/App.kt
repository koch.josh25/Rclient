package com.koch.josh.rclient

import android.app.Application
import com.koch.josh.rclient.helpers.AndroidRedditClient
import com.koch.josh.rclient.helpers.AndroidTokenStore
import net.dean.jraw.RedditClient
import net.dean.jraw.auth.AuthenticationManager
import net.dean.jraw.auth.RefreshTokenHandler
import net.dean.jraw.http.LoggingMode

/**
 * Created by joshkoch on 5/20/16.
 */
class App: Application() {
  override fun onCreate() {
    super.onCreate()
    var reddit: RedditClient = AndroidRedditClient(this)
    reddit.loggingMode = LoggingMode.ALWAYS
    AuthenticationManager.get().init(reddit, RefreshTokenHandler(AndroidTokenStore(this), reddit))
  }
}