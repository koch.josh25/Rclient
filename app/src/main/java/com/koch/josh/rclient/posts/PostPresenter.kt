package com.koch.josh.rclient.posts

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import net.dean.jraw.RedditClient
import net.dean.jraw.auth.AuthenticationManager
import net.dean.jraw.paginators.SubredditPaginator
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by Josh on 4/19/2016.
 */
class PostPresenter () : MvpBasePresenter<PostView>() {

  var subscription: Subscription? = null

  fun loadPosts(pullToRefresh: Boolean) {

    val reddit: RedditClient = AuthenticationManager.get().redditClient
    val frontPage = Observable.from(SubredditPaginator(reddit)).subscribeOn(Schedulers.io())

    view?.showLoading(pullToRefresh)

    subscription = frontPage.observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            {
              view?.setData(it)
            },

            {
              view?.showError(it, pullToRefresh)
            },
            {
              view?.showContent()
            }
        )
  }

  override fun attachView(view: PostView?) {
    super.attachView(view)
  }

  override fun detachView(retainInstance: Boolean) {

    super.detachView(retainInstance)
    if (!retainInstance) {
      subscription?.unsubscribe()
    }
  }
}