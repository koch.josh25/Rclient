package com.koch.josh.rclient

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.ViewGroup
import butterknife.bindView
import com.bluelinelabs.conductor.Conductor
import com.bluelinelabs.conductor.Router
import com.bluelinelabs.conductor.RouterTransaction
import com.koch.josh.rclient.posts.PostsController
import com.koch.josh.rclient.user.UserController
import net.dean.jraw.auth.AuthenticationManager
import net.dean.jraw.auth.AuthenticationState
import net.dean.jraw.http.oauth.OAuthException
import org.jetbrains.anko.async
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class MainActivity : AppCompatActivity() {
  private lateinit var mRouter: Router

  val mContainer: ViewGroup by bindView(R.id.controller_container)

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    mRouter = Conductor.attachRouter(this, mContainer, savedInstanceState)
    if (!mRouter.hasRootController()) {
      mRouter.setRoot(RouterTransaction.builder(PostsController()).build())
    }
  }

  override fun onBackPressed() {
    if (!mRouter.handleBack()) {
      super.onBackPressed();
    }
  }

  override fun onResume() {
    super.onResume()
    val state: AuthenticationState = AuthenticationManager.get().checkAuthState()

    when (state) {
      AuthenticationState.READY -> return
      AuthenticationState.NONE -> {
        toast("Log in first.")
        return
      }
      AuthenticationState.NEED_REFRESH -> {
        refreshAccessTokenAsync()
        return
      }
    }
  }

  private fun refreshAccessTokenAsync() {
    async() {
      try {
        AuthenticationManager.get().refreshAccessToken(UserController().CREDENTIALS)
      } catch(e: OAuthException) {
        Log.e("RedditApplication", "Could not refresh access token", e)
      }
      uiThread {
        Log.d("RedditApplication", "Reauthenticated")
      }
    }
  }
}
