package com.koch.josh.rclient

import android.text.TextUtils
import android.widget.ImageView
import com.squareup.picasso.Picasso

/**
 * Created by Josh on 4/20/2016.
 */

fun ImageView.loadImg(imageUrl: String) {
  if (TextUtils.isEmpty(imageUrl)) {
    Picasso.with(context).load(R.mipmap.ic_launcher).into(this)
  } else {
    Picasso.with(context).load(imageUrl).into(this)
  }
}