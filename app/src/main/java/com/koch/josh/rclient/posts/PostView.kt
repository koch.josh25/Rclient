package com.koch.josh.rclient.posts

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView
import net.dean.jraw.models.Listing
import net.dean.jraw.models.Submission

/**
 * Created by Josh on 4/19/2016.
 */
interface PostView : MvpLceView<List<Submission>> {
}