package com.koch.josh.rclient.comments

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView
import net.dean.jraw.models.Submission

/**
 * Created by joshkoch on 6/1/16.
 */
interface CommentView : MvpLceView<Submission>{
}