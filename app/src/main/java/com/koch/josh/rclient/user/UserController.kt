package com.koch.josh.rclient.user

import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.hannesdorfmann.mosby.mvp.conductor.MvpController
import com.koch.josh.rclient.R
import net.dean.jraw.auth.AuthenticationManager
import net.dean.jraw.http.oauth.Credentials
import net.dean.jraw.http.oauth.OAuthData
import net.dean.jraw.http.oauth.OAuthException
import net.dean.jraw.http.oauth.OAuthHelper
import org.jetbrains.anko.async

import java.net.URL

/**
 * Created by Josh on 5/4/2016.
 */
class UserController: UserView, MvpController<UserView, UserPresenter>() {

  final val CREDENTIALS: Credentials = Credentials.installedApp("D06DiC2nIkaFpQ", "https://koch.josh25.gitlab.io/plain-html/")

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup): View {
    val view: View = inflater.inflate(R.layout.controller_user, container, false)
    if (!AuthenticationManager.get().redditClient.isAuthenticated) {
      val helper: OAuthHelper = AuthenticationManager.get().redditClient.oAuthHelper
      //var scopes = arrayOf("identity","read")
      val authUrl: URL = helper.getAuthorizationUrl(CREDENTIALS, true, true, "identity", "read")

      val webView: WebView = view.findViewById(R.id.webview) as WebView
      webView.loadUrl(authUrl.toString())
      webView.setWebViewClient(object: WebViewClient() {
        override fun onPageStarted(view: WebView?, url: String, favicon: Bitmap?) {
          if(url.contains("code=")){
            onUserChallenge(url, CREDENTIALS)
            webView.destroy()
          }else if(url.contains("error=")) {
            Toast.makeText(activity, "You must press 'allow' to login with this account", Toast.LENGTH_SHORT).show()
            webView.loadUrl(authUrl.toExternalForm())
          }
        }
      })
    }
    return view
  }

  override fun createPresenter(): UserPresenter {
    return UserPresenter()
  }

  private fun onUserChallenge(url: String, creds: Credentials){
    async() {
      try {
        val data: OAuthData = AuthenticationManager.get().redditClient.oAuthHelper.onUserChallenge(url, creds)
        AuthenticationManager.get().redditClient.authenticate(data)
        AuthenticationManager.get().redditClient.authenticatedUser
      }catch(e: OAuthException) {
        Log.e("UserChallenge", "Could not log in", e)
      }
    }
  }

}